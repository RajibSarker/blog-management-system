﻿using BlogManagementSystem.Manager.Base;
using BlogManagementSystem.Manager.Contracts;
using BlogManagementSystem.Models;
using BlogManagementSystem.Repository.Contracts;

namespace BlogManagementSystem.Manager
{
    public class PostManager: Manager<Post>, IPostManager
    {
        private readonly IPostRepository _repository;

        public PostManager(IPostRepository repository):base(repository)
        {
            _repository = repository;
        }
    }
}
