﻿using BlogManagementSystem.Manager.Contracts;
using BlogManagementSystem.Repository.Contracts;
using System.Linq.Expressions;

namespace BlogManagementSystem.Manager.Base
{
    public abstract class Manager<T>: IManager<T> where T : class
    {
        IRepository<T> _repository;

        public Manager(IRepository<T> repository)
        {
            _repository = repository;
        }



        public virtual async Task<bool> RemoveAsync(T entity)
        {
            return await _repository.RemoveAsync(entity);
        }



        public virtual async Task<bool> AddAsync(T entity)
        {
            return await _repository.AddAsync(entity);
        }

        public virtual async Task<bool> UpdateAsync(T entity)
        {
            return await _repository.UpdateAsync(entity);
        }

        public virtual async Task<ICollection<T>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }


        public async Task<bool> AddRangeAsync(ICollection<T> entities)
        {
            return await _repository.AddRangeAsync(entities);
        }


        public async Task<bool> UpdateRangeAsync(ICollection<T> entity)
        {
            return await _repository.UpdateRangeAsync(entity);
        }


        public async Task<bool> RemoveRangeAsync(ICollection<T> entity)
        {
            return await _repository.RemoveRangeAsync(entity);
        }


        public async Task<T> GetByIdAsync(long id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task<ICollection<T>> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await _repository.GetAsync(predicate);
        }

        public async Task<T> GetFirstOrDefaultAsNoTrackingAsync(Expression<Func<T, bool>> predicate)
        {
            return await _repository.GetFirstOrDefaultAsNoTrackingAsync(predicate);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _repository.SaveChangesAsync();
        }
    }
}
