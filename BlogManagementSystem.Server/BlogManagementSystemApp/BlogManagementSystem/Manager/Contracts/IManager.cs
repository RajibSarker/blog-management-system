﻿using System.Linq.Expressions;

namespace BlogManagementSystem.Manager.Contracts
{
    public interface IManager<T> where T : class
    {
        Task<bool> AddAsync(T entity);
        Task<bool> UpdateAsync(T entity);
        Task<bool> RemoveAsync(T entity);
        Task<T> GetByIdAsync(long id);
        Task<T> GetFirstOrDefaultAsNoTrackingAsync(Expression<Func<T, bool>> predicate);
        Task<ICollection<T>> GetAllAsync();
        Task<bool> SaveChangesAsync();
        Task<bool> AddRangeAsync(ICollection<T> entities);
        Task<bool> UpdateRangeAsync(ICollection<T> entity);
        Task<bool> RemoveRangeAsync(ICollection<T> entity);
        Task<ICollection<T>> GetAsync(Expression<Func<T, bool>> predicate);
    }
}
