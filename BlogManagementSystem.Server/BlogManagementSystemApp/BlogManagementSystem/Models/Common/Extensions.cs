﻿using BlogManagementSystem.Models.Middleware;

namespace BlogManagementSystem.Models.Common
{
    public static class Extensions
    {
        public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomExceptionHandlerMiddleware>();
        }

        public static IApplicationBuilder UserLoggingHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggerAndPerformanceMiddleware>();
        }

        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
        }

        //public static void AddPagination(this HttpResponse response, int currentPage, int itemPerPage, int totalItems, int totalPages)
        //{
        //    var paginationHeader = new PaginationHeader(currentPage, itemPerPage, totalItems, totalPages);
        //    var camelCaseFormatter = new JsonSerializerSettings();
        //    camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
        //    response.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormatter));
        //    response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        //}
        //public static void AddResponseFileName(this HttpResponse response, string fileName)
        //{
        //    var camelCaseFormatter = new JsonSerializerSettings();
        //    camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
        //    response.Headers.Add("FileName", JsonConvert.SerializeObject(fileName, camelCaseFormatter));
        //    response.Headers.Add("Access-Control-Expose-Headers", "FileName");
        //}

    }
}
