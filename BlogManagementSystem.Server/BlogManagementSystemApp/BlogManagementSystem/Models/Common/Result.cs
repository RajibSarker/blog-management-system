﻿namespace BlogManagementSystem.Models.Common
{
    public class Result
    {
        internal Result(bool succeeded, IEnumerable<string> errors, string success, long errorHeaderId = 0)
        {
            Succeeded = succeeded;
            Errors = errors.ToArray();
            Message = success;
            ErrorHeaderId = errorHeaderId;
        }

        internal Result(bool succeeded, string success, long logId = 0)
        {
            Succeeded = succeeded;
            Message = success;
            LogId = logId;
        }

        public bool Succeeded { get; set; }

        public string Message { get; set; }

        public string[] Errors { get; set; }
        public long ErrorHeaderId { get; set; }
        public long LogId { get; set; }

        public static Result Success(string success = "Success")
        {
            return new Result(true, new string[] { }, success);
        }

        public static Result Failure(IEnumerable<string> errors)
        {
            return new Result(false, errors, null);
        }

        public static Result FailureWithError(IEnumerable<string> errors, long errorHeaderId = 0)
        {
            return new Result(false, errors, null, errorHeaderId);
        }

        public static Result SuccessWithLog(string success = "Success", long logId=0)
        {
            return new Result(true, success, logId);
        }
    }
}
