﻿using AutoMapper;
using BlogManagementSystem.Models.DTO;

namespace BlogManagementSystem.Models.AutoMapper
{
    public class MappingProfiles:Profile
    {
        public MappingProfiles()
        {
            CreateMap<PostRequestDto, Post>();
            CreateMap<Post, PostRequestDto>();
        }
    }
}
