﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlogManagementSystem.Models
{
    [Table("POSTS")]
    public class Post: AuditableEntity
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Summary { get; set; }
        public string UrlHandle { get; set; }
        public string FeaturedUrl { get; set; }
        public bool IsVisible { get; set; }
        public string Author { get; set; }
    }
}
