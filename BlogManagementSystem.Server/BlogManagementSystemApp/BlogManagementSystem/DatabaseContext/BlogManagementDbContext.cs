﻿using BlogManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogManagementSystem.DatabaseContext
{
    public class BlogManagementDbContext : DbContext
    {
        public BlogManagementDbContext(DbContextOptions options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        #region tables
        public DbSet<Post> Posts { get; set; }
        #endregion

        //public static BlogManagementDbContext Create(IConfiguration configuration)
        //{
        //    var optionsBuilder = new DbContextOptionsBuilder<BlogManagementDbContext>();
        //    optionsBuilder.UseNpgsql(configuration.GetConnectionString("AppConnectionString"));
        //    var context = new BlogManagementDbContext(optionsBuilder.Options);
        //    return context;
        //}
    }
}
