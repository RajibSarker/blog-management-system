﻿using BlogManagementSystem.DatabaseContext;
using BlogManagementSystem.Models;
using BlogManagementSystem.Repository.Base;
using BlogManagementSystem.Repository.Contracts;

namespace BlogManagementSystem.Repository
{
    public class PostRepository: Repository<Post>, IPostRepository
    {
        private readonly BlogManagementDbContext _db;

        public PostRepository(BlogManagementDbContext db):base(db)
        {
            _db = db;
        }
        //public override async Task<bool> UpdateAsync(Post entity)
        //{
        //    _db.Posts.Update(entity);
        //    return await _db.SaveChangesAsync() > 0;
        //}
    }
}
