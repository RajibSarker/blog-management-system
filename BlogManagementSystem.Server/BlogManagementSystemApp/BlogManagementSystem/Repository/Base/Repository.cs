﻿using BlogManagementSystem.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Data;
using System.Linq.Expressions;

namespace BlogManagementSystem.Repository.Base
{
    public abstract class Repository<T>: IRepository<T> where T: class
    {
        DbContext _dbContext;

        DbSet<T> Table
        {
            get
            {
                return _dbContext.Set<T>();
            }
        }

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<bool> AddAsync(T entity)
        {
            await Table.AddAsync(entity);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        //public async Task<bool> BatchInsertAsync(ICollection<T> entities, Func<DbContext> contextCreator)
        //{
        //    try
        //    {
        //        if (entities == null || !entities.Any())
        //        {
        //            return false;
        //        }

        //        int batchSize = 1000;
        //        int totalCount = entities.Count();
        //        //using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
        //        //{

        //        DbContext context = contextCreator.Invoke();
        //        context.ChangeTracker.AutoDetectChangesEnabled = false;

        //        int totalPageSize = GetTotalPageSize(totalCount, batchSize);

        //        for (int start = 0; start < totalPageSize; start++)
        //        {
        //            var addeableEntities = entities.Skip(start * batchSize).Take(batchSize).ToList();
        //            context = await context.AddToContextAsync<T>(addeableEntities, true,
        //                           contextCreator);
        //        }
        //        await context.SaveChangesAsync();


        //        //await context.SaveChangesAsync();
        //        //    transactionScope.Complete();

        //        //}

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        private int GetTotalPageSize(int totalCount, int batchSize)
        {

            // include this if when you always want at least 1 page 
            if (totalCount == 0)
            {
                return 1;
            }

            return totalCount % batchSize != 0
                ? totalCount / batchSize + 1
                : totalCount / batchSize;
        }

        public virtual async Task<bool> RemoveAsync(T entity)
        {
            Table.Remove(entity);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRangeAsync(ICollection<T> entity)
        {
            Table.RemoveRange(entity);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await Table.FindAsync(id);
        }

        public async Task<T> GetByIdAsync(long id)
        {
            return await Table.FindAsync(id);
        }

        public virtual bool UpdateRange(ICollection<T> entity)
        {
            Table.UpdateRange(entity);

            return _dbContext.SaveChanges() > 0;
        }

        public virtual bool RemoveRange(ICollection<T> entity)
        {
            Table.RemoveRange(entity);
            return _dbContext.SaveChanges() > 0;
        }


        public virtual async Task<bool> UpdateAsync(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> UpdateRangeAsync(ICollection<T> entity)
        {
            Table.UpdateRange(entity);

            return await _dbContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<ICollection<T>> GetAllAsync()
        {
            return await Table.ToListAsync();
        }

        public virtual async Task<ICollection<T>> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await Table.Where(predicate).ToListAsync();
        }

        public virtual async Task<bool> AddRangeAsync(ICollection<T> entities)
        {
            await _dbContext.Set<T>().AddRangeAsync(entities);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public Task<bool> BatchInsertAsync(ICollection<T> entities, Func<DbContext> contextCreator)
        {
            throw new NotImplementedException();
        }

        public async Task<T> GetFirstOrDefaultAsNoTrackingAsync(Expression<Func<T, bool>> predicate)
        {
            return await Table.AsNoTracking().FirstOrDefaultAsync(predicate);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
