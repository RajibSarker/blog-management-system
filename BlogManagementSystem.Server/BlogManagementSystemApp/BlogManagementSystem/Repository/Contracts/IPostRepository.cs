﻿using BlogManagementSystem.Models;

namespace BlogManagementSystem.Repository.Contracts
{
    public interface IPostRepository: IRepository<Post>
    {
    }
}
