﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BlogManagementSystem.Repository.Contracts
{
    public interface IRepository<T> where T : class
    {
        Task<bool> AddAsync(T entity);
        Task<bool> AddRangeAsync(ICollection<T> entities);
        Task<bool> UpdateAsync(T entity);
        Task<bool> UpdateRangeAsync(ICollection<T> entity);
        Task<bool> RemoveAsync(T entity);
        Task<bool> RemoveRangeAsync(ICollection<T> entity);
        Task<T> GetByIdAsync(long id);
        Task<bool> SaveChangesAsync();
        Task<T> GetFirstOrDefaultAsNoTrackingAsync(Expression<Func<T, bool>> predicate);
        Task<ICollection<T>> GetAllAsync();
        Task<ICollection<T>> GetAsync(Expression<Func<T, bool>> predicate);
        Task<bool> BatchInsertAsync(ICollection<T> entities, Func<DbContext> contextCreator);
    }
}
