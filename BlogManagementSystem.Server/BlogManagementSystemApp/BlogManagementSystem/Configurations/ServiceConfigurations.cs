﻿using BlogManagementSystem.DatabaseContext;
using BlogManagementSystem.Manager;
using BlogManagementSystem.Manager.Contracts;
using BlogManagementSystem.Repository;
using BlogManagementSystem.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace BlogManagementSystem.Configurations
{
    public class ServiceConfigurations
    {
        public static void Configuration(IServiceCollection service, IConfiguration configuration)
        {
            // npgsql configurations
            service.AddDbContext<BlogManagementDbContext>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("AppConnectionString"));
            });

            // registered services
            service.AddTransient<IPostManager, PostManager>();
            service.AddTransient<IPostRepository, PostRepository>();
        }
    }
}
