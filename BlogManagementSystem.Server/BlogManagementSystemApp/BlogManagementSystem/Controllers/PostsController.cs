﻿using AutoMapper;
using BlogManagementSystem.Manager.Contracts;
using BlogManagementSystem.Models;
using BlogManagementSystem.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogManagementSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : ControllerBase
    {
        private readonly IPostManager _postManager;
        private readonly IMapper _mapper;

        public PostsController(IPostManager postManager, IMapper mapper)
        {
            _postManager = postManager;
            _mapper = mapper;
        }

        [HttpGet("getPosts")]
        public async Task<IActionResult> GetPosts()
        {
            return Ok(await _postManager.GetAsync(c => c.IsVisible == true));
        }

        [HttpGet("getPost/{id}/post")]
        [ActionName("getPost")]
        public async Task<IActionResult> GetPost(long id)
        {
            if (id <= 0) return BadRequest("Invalid input request.");

            return Ok(await _postManager.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostRequestDto post)
        {
            if (post == null) return BadRequest("Invalid input request.");

            var data = _mapper.Map<Post>(post);
            data.CreatedOn = DateTime.Now;
            var isSaved = await _postManager.AddAsync(data);
            if (isSaved)
            {
                return CreatedAtAction("getPost", new { id = data.Id }, data);
            }

            return BadRequest("Sorry! post could not be created.");
        }

        [HttpPut("id")]
        public async Task<IActionResult> Put(long id, [FromBody] PostRequestDto postData)
        {
            if (postData == null || id<=0) return BadRequest("Invalid Input Request.");

            var existingPost = await _postManager.GetFirstOrDefaultAsNoTrackingAsync(c => c.Id == id);
            if (existingPost == null) return NotFound($"No post is found for id: {id}.");

            existingPost = _mapper.Map(postData, existingPost);
            var isUpdated = await _postManager.UpdateAsync(existingPost);
            if (isUpdated)
            {
                return CreatedAtAction("getPost", new { id = existingPost.Id }, existingPost);
            }

            return BadRequest("Sorry! post could not be updated.");
        }

        [HttpDelete("id")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id <= 0) return BadRequest("Invalid input request.");

            var existingPost = await _postManager.GetByIdAsync(id);
            if (existingPost == null) return NotFound($"No post is found for id: {id}");

            var isDeleted = await _postManager.RemoveAsync(existingPost);
            return Ok(isDeleted);
        }
    }
}
