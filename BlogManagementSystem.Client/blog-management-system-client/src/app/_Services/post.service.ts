import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Post } from '../_Models/post';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  )
};
@Injectable({
  providedIn: 'root'
})
export class PostService {
  apiUrl = environment.baseApiUrl

  constructor(private http: HttpClient) { }

  addNewPost(post: Post) {
    return this.http.post(this.apiUrl + 'Posts', post, { headers: httpOptions.headers });
  }

}
