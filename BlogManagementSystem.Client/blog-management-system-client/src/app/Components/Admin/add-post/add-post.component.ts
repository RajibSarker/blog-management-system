import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { timeStamp } from 'console';
import { Post } from 'src/app/_Models/post';
import { PostService } from 'src/app/_Services/post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  post: Post = new Post();
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image', 'video']                         // link and image, video
    ]
  };
  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.post) {
      debugger
      this.post.author = 'Rajib Sarker';
      this.postService.addNewPost(this.post).subscribe(res => {
        console.log(res);

      }, error => {
        console.log(error);

      })
    }
  }

  getEditorInstance(editorInstance: any) {
    console.log(editorInstance)
    let toolbar = editorInstance.getModule('toolbar');
    toolbar.addHandler('image', this.showImageUI);
  }
  showImageUI() {
    console.log('Handle image');
  }
}
