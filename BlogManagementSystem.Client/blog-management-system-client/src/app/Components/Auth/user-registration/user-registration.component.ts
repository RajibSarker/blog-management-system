import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_Models/user';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  user: User = new User();
  constructor() { }

  ngOnInit() {
  }

  onSubmit(){
    console.log('User: ', this.user);
  }
}
