import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPostComponent } from './Components/Admin/add-post/add-post.component';


const routes: Routes = [
  { path: 'post/create', component: AddPostComponent, pathMatch: 'full' },
  { path: '', redirectTo: 'post/create', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
