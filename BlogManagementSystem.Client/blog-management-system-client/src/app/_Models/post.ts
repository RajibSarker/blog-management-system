export class Post {
    id: number = 0;
    title: string;
    content: string = '';
    summary: string = '';
    urlHandle: string = '';
    featuredUrl: string = '';
    isVisible: boolean;
    author: string;
}